#include "Detail.h"

Detail::Detail() {
	m_name = "-";
	m_type = Type::NO_TYPE;
}

Detail::Detail(string name) {
	m_name = name;
	m_type = Type::NO_TYPE;
}

Detail::Detail(string name, Type type) {
	m_name = name;
	m_type = type;
}


Detail::~Detail() {

}