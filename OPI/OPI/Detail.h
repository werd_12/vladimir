#pragma once
#include <string>
#include "DetailsType.h"
using namespace std;

class Detail {
public:
	Detail();
	Detail(string name);
	Detail(string name, Type type);
	~Detail();

private:
	string m_name;
	Type   m_type;
};

