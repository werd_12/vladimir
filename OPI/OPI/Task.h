#pragma once
#include <string>
using namespace std;

class Task {
public:
	Task();
	Task(string name, int id_human);
	~Task();

private:
	string m_name_task;
	int    m_id_human;
};


