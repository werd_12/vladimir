﻿#include "Human.h"
#include <iostream>
//using namespace BKM;

int Human::s_idGenerator = 1;

//Конструкторы и детсрукторы ======================================================================
Human::Human() {
	this->m_firstName;
	this->m_lastName;
	this->m_login;
	this->m_password;
	//this->m_id = 0;
		
			//s_idGenerator++;
			//++s_countPeople;
		
}

Human::Human(string firstName, string lastName, string login, string password) {
	this->m_login = login;
	this->m_password = password;
	this->m_firstName = firstName;
	this->m_lastName = lastName;
		//this->m_id        =	1;
		
			//s_idGenerator++;
			//++s_countPeople;
		
}
Human::Human(string login, string password) {
	this->m_login = login;
	this->m_password = password;
	this->m_firstName = "-";
	this->m_lastName = "-";
	this->m_id = 0;
	
			//s_idGenerator++;
		//++s_countPeople;
		
}

Human::Human(const Human & other) {
	this->m_firstName = other.m_firstName;
	this->m_lastName = other.m_lastName;
	this->m_login = other.m_login;
	this->m_password = other.m_password;
	this->m_id = other.m_id;
	
}
Human::~Human() {
		//--s_countPeople;
}

// Методы =========================================================================================

void Human::Print() const {
	cout << "Èìÿ:    \t" << this->m_firstName << endl;
	cout << "Фамилия:\t" << this->m_lastName << endl;
	cout << "Логин:  \t" << this->m_login << endl;
	cout << "Пароль: \t" << this->m_password << endl;
	cout << "Id:	 \t" << this->m_id << endl;
	
}
void Human::set() {
	cin.ignore();
	cout << "Введите имя:     \t"; cin >> m_firstName;
	cout << "¬ведите фамилию: \t"; cin >> m_lastName;
cout << "¬ведите логин:   \t"; cin >> m_login;
	cout << "¬ведите пароль:  \t"; cin >> m_password;
	
}

// Перегрузка операторов ==========================================================================

Human & Human::operator=(const Human & human) {
	this->m_firstName = human.m_firstName;
	this->m_lastName = human.m_lastName;
	this->m_login = human.m_login;
	this->m_password = human.m_password;
	this->m_id = human.m_id;
	
		return *this;
	
}

bool Human::operator>(const Human & human) {
	bool correct{};
	if (this->m_lastName > human.m_lastName) {
		correct = true;
		
	}
	 else if (this->m_lastName == human.m_lastName) {
		if (this->m_firstName > human.m_firstName) {
			correct = true;
			
		}
		 else if (this->m_firstName == human.m_firstName) {
			if (this->m_id > human.m_id) {
				correct = true;
				
			}
			
		}
		 else correct = false;
		
	}
	 else correct = false;
	return correct;
	
}

string Human::getLogin() {
	return m_login;
	
}

string Human::getPassword() {
	return m_password;
	
}


void Human::setLogin(string login) {
m_login = login;
}

void Human::setPassword(string password) {
	m_password = password;
}

void Human::setID(int id) {
	m_id = id;
	
}


ostream & operator<<(ostream & out, Human & human) {
	out << "Èìÿ:    \t" << human.m_firstName << "\n"
		 << "Фамилия:\t" << human.m_lastName << "\n"
		 << "Логин:  \t" << human.m_login << "\n"
		 << "Пароль: \t" << human.m_password << "\n"
		 << "Id:	    \t" << human.m_id << "\n\n";
	return out;
	
}

istream & operator>>(istream & in, Human & human) {
	human.set();
	return in;
	
}